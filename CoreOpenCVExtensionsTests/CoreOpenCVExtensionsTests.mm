//
//  CoreOpenCVExtensionsTests.m
//  CoreOpenCVExtensionsTests
//
//  Created by JiangZhping on 2017/1/31.
//  Copyright © 2017年 JiangZhping. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <CoreOpenCVExtensions/CoreOpenCVExtensions.h>

@interface CoreOpenCVExtensionsTests : XCTestCase

@end

@implementation CoreOpenCVExtensionsTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testPreferenceManagerPersistence {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
    debugcout(PreferenceManagerSingleton::getInstance().getInternalDataString());
//    PreferenceManager::getInstance().persist("dMat", cv::Mat(cv::Matx33d::eye()));
//    debugcout(PreferenceManager::getInstance().internalDataString);
//    PreferenceManager::getInstance().persist("cMat", cv::Mat(cv::Matx33d::eye()));
//    debugcout(PreferenceManager::getInstance().internalDataString);
//    PreferenceManager::getInstance().persist("cMat", cv::Mat(cv::Matx33d::eye() * 5));
//    debugcout(PreferenceManager::getInstance().internalDataString);
//    PreferenceManager::getInstance().flushToDisk();
}

- (void) testExecCommand {
    std::string result = crossPlatform_exec("ls");
    std::cout<<"result"<<result<<std::endl;
}

- (void) testPreferenceManagerQueue {
    PreferenceManagerSingleton::getInstance().persist("aFloatValue", 5.5f);
    float aFloatValue = PreferenceManagerSingleton::getInstance().queueFor<float>("aFloatValue");
    XCTAssertEqual(aFloatValue, 5.5f);
    
    PreferenceManagerSingleton::getInstance().persist("aRect", cv::Rect2f(0.5, 0.5, 123, 123));
    cv::Rect2f aRectValue = PreferenceManagerSingleton::getInstance().queueFor<cv::Rect2f>("aRect");
    XCTAssertEqual(aRectValue.tl(), cv::Point2f(0.5, 0.5));
    
    cv::Rect2f bRectValue = PreferenceManagerSingleton::getInstance().queueFor("bRect", cv::Rect2f(0.7, 0.5, 123, 123));
    XCTAssertEqual(bRectValue.tl(), cv::Point2f(0.7, 0.5));
    
//    cv::Rect2f cRectValue = PreferenceManager::getInstance().queueFor<cv::Rect2f>("cRect");
    
    debugcout(PreferenceManagerSingleton::getInstance().getInternalDataString());
}

@end
