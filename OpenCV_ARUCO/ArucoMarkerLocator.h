
#ifndef ArucoMarkerLocator_h
#define ArucoMarkerLocator_h

#include <CoreOpenCVExtensions/CoreOpenCVExtensions.h>
#include "aruco.hpp"

using namespace std;
using namespace cv;
namespace cve {
    
	void printArucoMarkerToFile(int markerFamilyId, int markerId, int size, const string & filename);

	void detectArucoMarkersAndPose(const Mat & _imageIn, float markerLength, const cv::Matx33d & _cameraMatrix, const Vec5d & _distCoeffs, vector< int > & _ids, vector< vector< Point2f > > &_corners, vector< Vec3d > & _rvecs, vector< Vec3d > & _tvecs, float minMarkerPerimeterRate = 0.04, float maxMarkerPerimeterRate = 0.2, int markerFamilyId = 0);
}


#endif
