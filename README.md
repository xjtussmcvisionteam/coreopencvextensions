# Project: Core OpenCV Extensions

This is basically a collection of various of utility methods directly and only directly built upon OpenCV.

The project is built simultaneously for iOS/OSX.

It is a Xcode 8 project targeting on macOS 10.12 and iOS 10.

The OpenCV dependency is a opencv2.framework file (built using the official script).