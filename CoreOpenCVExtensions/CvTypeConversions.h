//
//  CvTypeConversions.h
//  CoreOpenCVExtensions
//
//  Created by JiangZhping on 2016/9/29.
//  Copyright © 2016年 JiangZhiping. All rights reserved.
//

#ifndef jzplib_analysis_hpp
#define jzplib_analysis_hpp

#include <opencv2/opencv.hpp>

namespace cve {
    
}

template<typename typeOfMat> std::vector<cv::Point_<typeOfMat>> CvPointVectorMakeWithCvMatNx2(const cv::Mat_<typeOfMat> & mat) {
    std::vector<cv::Point_<typeOfMat>> resultVector;
    for (int i = 0; i < mat.rows; i++) {
        resultVector.push_back(cv::Point_<typeOfMat>(mat.template at<typeOfMat>(i,0), mat.template at<typeOfMat>(i,1)));
    }
    return resultVector;
}

template<typename typeOfMat> std::vector<cv::Point3_<typeOfMat>> CvPoint3VectorMakeWithCvMatNx3(const cv::Mat_<typeOfMat> & mat) {
    std::vector<cv::Point3_<typeOfMat>> resultVector;
    for (int i = 0; i < mat.rows; i++) {
        resultVector.push_back(cv::Point3_<typeOfMat>(mat.template at<typeOfMat>(i,0), mat.template at<typeOfMat>(i,1), mat.template at<typeOfMat>(i,2)));
    }
    return resultVector;
}

template<typename _Tp> cv::Mat_<_Tp> CvMatNx2MakeWithCvPointVector(const std::vector<cv::Point_<_Tp>> & vector) {
    cv::Mat result = cv::Mat(vector).reshape(1);
    return result;
}

template<typename _Tp> cv::Mat_<_Tp> CvMatNx3MakeWithCvPoint3Vector(const std::vector<cv::Point3_<_Tp>> & vector) {
    cv::Mat result = cv::Mat(vector).reshape(1);
    return result;
}

template<typename _Tp> cv::Mat1d CvMatMakeWithCvPoint(const cv::Point_<_Tp> & point) {
    return cv::Mat1d(2,1)<< (double)point.x, (double)point.y;
}

template<typename _Tp> cv::Mat1d CvMatMakeWithCvPoint3(const cv::Point3_<_Tp> & point) {
    return cv::Mat1d(3,1)<< (double)point.x, (double)point.y, (double)point.z;
}

template<typename _Tp> cv::Point_<_Tp> CvPointMakeWithCvMat(const cv::Mat_<_Tp> & pointMat) {
    return cv::Point_<_Tp>(pointMat.template at<_Tp>(0), pointMat.template at<_Tp>(1));
}

template<typename _Tp> cv::Point3_<_Tp> CvPoint3MakeWithCvMat(const cv::Mat_<_Tp> & pointMat) {
    return cv::Point3_<_Tp>(pointMat.template at<_Tp>(0), pointMat.template at<_Tp>(1), pointMat.template at<_Tp>(2));
}

#endif
