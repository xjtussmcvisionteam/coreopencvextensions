//
//  OpencvMatxExtensions.hpp
//  CoreOpenCVExtensions
//
//  Created by JiangZhping on 2017/1/7.
//  Copyright © 2017年 JiangZhping. All rights reserved.
//

#ifndef OpencvMatxExtensions_hpp
#define OpencvMatxExtensions_hpp

#include <opencv2/opencv.hpp>

namespace cv {
    typedef cv::Matx<float, 1, 5> Matx15f;
    typedef cv::Matx<double, 1, 5> Matx15d;
    typedef cv::Vec<float, 5> Vec5f;
    typedef cv::Vec<double, 5> Vec5d;
}

#pragma mark - cve::matx
namespace cve { namespace matx {
            
    /**
     extract subMatrix from matx via C++ template
     
     @param fromMat the mat for extracting subMatrix
     @param _To to result Matx type
     @param rowStart subMatrix rowStart
     @param rowEnd subMatrix rowEnd
     @param colStart subMatrix colStart
     @param colEnd subMatrix colEnd
     @return a Matx of type cv::Matx<_To, rowEnd - rowStart, colEnd - colStart>
     */
    template<typename _To, int rowStart, int rowEnd, int colStart, int colEnd, int m, int n, typename _From> cv::Matx<_To, rowEnd - rowStart, colEnd - colStart> submatrix(cv::Matx<_From, m, n> fromMat);
    
    /**
     extract subMatrix from matx via C++ template. This method invokes the fully functional cve::matx::submatrix method except not changing output type.
     */
    template<int rowStart, int rowEnd, int colStart, int colEnd, int m, int n, typename _From> cv::Matx<_From, rowEnd - rowStart, colEnd - colStart> submatrix(cv::Matx<_From, m, n> fromMat);
    
    /**
     change the Matx type to _To type
     
     @param fromMat the matx to be type-converted
     @param _To the destination type
     @return fromMat in type _To
     */
    template<typename _To, int m, int n, typename _From> cv::Matx<_To, m, n> changeType(cv::Matx<_From, m, n> fromMat);
    
    
    /**
     copy a cv::Mat_<Type> to cv::Matx<Type, m ,n>

     @param sourceMat the source cv::Mat to be copied from
     @param m the rows of result matx, can be smaller than the sourceMat
     @param n the cols of result matx, can be smaller than the sourceMat
     @return the coppied cv::Matx<Type, m ,n> with the same content of sourceMat
     */
    template<int m, int n, typename _Tp> cv::Matx<_Tp, m, n> fromMat_(const cv::Mat_<_Tp> & sourceMat);
    
    /**
     copy a cv::Mat_<Type> to cv::Matx<Type, m ,n>. This variant supports multi-channel mat.
     
     @param sourceMat the source cv::Mat to be copied from
     @param channel the sourceMat's channel to be coppied from, default = 0
     @param m the rows of result matx, can be smaller than the sourceMat
     @param n the cols of result matx, can be smaller than the sourceMat
     @return the coppied cv::Matx<Type, m ,n> with the same content of sourceMat
     */
    template<int m, int n, int channel, typename _Tp> cv::Matx<_Tp, m, n> fromMat_(const cv::Mat_<cv::Vec<_Tp, channel>> & sourceMat, int targetChannel = 0);
    
    /**
     copy a cv::Mat to cv::Matx<Type, m ,n>, similar to fromMat_, but with explicit typing.

     @param sourceMat the source cv::Mat to be copied from
     @param channel the sourceMat's channel to be coppied from, default = 0
     @param _Tp the type of Matx, also used for element accessing in sourceMat
     @param m the rows of result matx, can be smaller than the sourceMat
     @param n the cols of result matx, can be smaller than the sourceMat
     @return the coppied cv::Matx<Type, m ,n> with the same content of sourceMat
     */
    template<typename _Tp, int m, int n> cv::Matx<_Tp, m, n> fromMat(const cv::Mat & sourceMat, int channel = 0);

} }


#pragma mark - cve::vec
namespace cve { namespace vec {
    
    /**
     Return a new cv::Vec value with targetDimension dimension. The content is copied from originalVec. It originalDimension < targetDimension, appending the tail with pendingValue.
     
     @param originalVec the cv::Vec whose dimension is to be changed.
     @param targetDimension the dimension of the returned cv::Vec
     @param pendingValue if originalDimension < targetDimension, pendingValue is to be appended to the returned cv::Vec. The default value is (_Tp)0.
     @return a new cv::Vec value whose content is copied from originalVec, and its dimension is targetDimension.
     */
    template<int targetDimension, int originalDimension, typename _Tp> cv::Vec<_Tp, targetDimension> changeDimension(const cv::Matx<_Tp, originalDimension, 1> & originalVec, _Tp pendingValue = (_Tp)0.0 );
    
    /**
     make cv::Vec<3> from cv::Point3
     
     @param point a cv::Point3 value to be copied to cv::Vec
     @return a cv::Vec<3> with the same content of point
     */
    template<typename _Tp> cv::Vec<_Tp,3> fromPoint3(const cv::Point3_<_Tp> & point);
    
    /**
     make cv::Vec<2> from cv::Point2
     
     @param point a cv::Point2 value to be copied to cv::Vec
     @return a cv::Vec<2> with the same content of point
     */
    template<typename _Tp> cv::Vec<_Tp,2> fromPoint(const cv::Point_<_Tp> & point);
    
    /**
     make a cv::Point value with the same content of the input cv::Vec.
     
     @param vec source cv::Vec
     @param pendingValue if vec's dimension is smaller than 2, appends pendingValue to the ending.
     @return a cv::Point with the same value of vec.
     */
    template<typename _DestTp, typename _Tp, int sourceDimension> cv::Point_<_DestTp> toPoint(const cv::Vec<_Tp, sourceDimension> & vec);
    
    /**
     make a cv::Point3 value with the same content of the input cv::Vec.
     
     @param vec source cv::Vec
     @param pendingValue if vec's dimension is smaller than 3, appends pendingValue to the ending.
     @return a cv::Point3 with the same value of vec.
     */
    template<typename _DestTp, typename _Tp, int sourceDimension> cv::Point3_<_DestTp> toPoint3(const cv::Vec<_Tp, sourceDimension> & vec);
    
    /**
     convert a cv::Matx to cv::Vec.
     
     @param sourceMatx a m*n matx of type _Tp
     @return a cv::Vec of m*n length
     */
    template<typename _Tp, int m, int n> cv::Vec<_Tp, m * n> fromMatx(const cv::Matx<_Tp, m, n> & sourceMatx);
} }


#pragma mark - Implementations

namespace cve { namespace matx {
    
    template<typename _To, int rowStart, int rowEnd, int colStart, int colEnd, int m, int n, typename _From> cv::Matx<_To, rowEnd - rowStart, colEnd - colStart> submatrix(cv::Matx<_From, m, n> fromMat) {
        cv::Matx<_To, rowEnd - rowStart, colEnd - colStart> subMatx;
        for (int i = rowStart; i < rowEnd ; i++) {
            for (int j = colStart ; j < colEnd; j ++) {
                subMatx(i - rowStart , j - colStart) = (_To)fromMat(i,j);
            }
        }
        
        return subMatx;
    }

    template<int rowStart, int rowEnd, int colStart, int colEnd, int m, int n, typename _From> cv::Matx<_From, rowEnd - rowStart, colEnd - colStart> submatrix(cv::Matx<_From, m, n> fromMat) {
        return submatrix<_From, rowStart, rowEnd, colStart, colEnd>(fromMat);
    }
    
    template<typename _To, int m, int n, typename _From> cv::Matx<_To, m, n> changeType(cv::Matx<_From, m, n> fromMat) {
        cv::Matx<_To, m, n> toMatx;
        for (int i = 0 ; i < m * n ; i ++) {
            toMatx.val[i] = (_To) fromMat.val[i];
        }
        return toMatx;
    }
    
    template<int m, int n, typename _Tp> cv::Matx<_Tp, m, n> fromMat_(const cv::Mat_<_Tp> & sourceMat) {
        cv::Matx<_Tp, m, n> toMatx;
        for (int i = 0 ; i < m ; i ++) {
            for (int j = 0 ; j < n ; j ++) {
                toMatx(i,j) = sourceMat(i,j);
            }
        }
        return toMatx;
    }
    
    template<int m, int n, int channel, typename _Tp> cv::Matx<_Tp, m, n> fromMat_(const cv::Mat_<cv::Vec<_Tp, channel>> & sourceMat, int targetChannel) {
        cv::Matx<_Tp, m, n> toMatx;
        for (int i = 0 ; i < m ; i ++) {
            for (int j = 0 ; j < n ; j ++) {
                toMatx(i,j) = sourceMat(i,j)(targetChannel);
            }
        }
        return toMatx;
    }
    
    template<typename _Tp, int m, int n> cv::Matx<_Tp, m, n> fromMat(const cv::Mat & sourceMat, int channel) {
        cv::Matx<_Tp, m, n> toMatx;
        for (int i = 0 ; i < m ; i ++) {
            for (int j = 0 ; j < n ; j ++) {
                toMatx(i,j) =  * (sourceMat.template ptr<_Tp>(i, j) + channel);
            }
        }
        return toMatx;
    }
} }

namespace cve { namespace vec {
    template<int targetDimension, int originalDimension, typename _Tp> cv::Vec<_Tp, targetDimension> changeDimension(const cv::Matx<_Tp, originalDimension, 1> & originalVec, _Tp pendingValue) {
        cv::Vec<_Tp, targetDimension> resultValue;
        memcpy(resultValue.val, originalVec.val, std::min(originalDimension, targetDimension) * sizeof(_Tp));
        if (targetDimension > originalDimension) {
            for (int i = originalDimension; i < targetDimension; i ++) {
                resultValue(i) = pendingValue;
            }
        }
        return resultValue;
    }
    
    template<typename _Tp> cv::Vec<_Tp,2> fromPoint(const cv::Point_<_Tp> & point) {
        return cv::Vec<_Tp,2>(point.x, point.y);
    }
    
    template<typename _Tp> cv::Vec<_Tp,3> fromPoint3(const cv::Point3_<_Tp> & point) {
        return cv::Vec<_Tp,3>(point.x, point.y, point.z);
    }
    
    template<typename _DestTp, typename _Tp, int sourceDimension> cv::Point_<_DestTp> toPoint(const cv::Vec<_Tp, sourceDimension> & sourceVec) {
        return cv::Point_<_DestTp>(cv::saturate_cast<_DestTp>(sourceVec(0)), cv::saturate_cast<_DestTp>(sourceVec(1)));
    }
    
    template<typename _DestTp, typename _Tp, int sourceDimension> cv::Point3_<_DestTp> toPoint3(const cv::Vec<_Tp, sourceDimension> & sourceVec) {
        return cv::Point3_<_DestTp>(cv::saturate_cast<_DestTp>(sourceVec(0)), cv::saturate_cast<_DestTp>(sourceVec(1)), cv::saturate_cast<_DestTp>(sourceVec(2)));
    }
    
    template<typename _Tp, int m, int n> cv::Vec<_Tp, m * n> fromMatx(const cv::Matx<_Tp, m, n> & sourceMatx) {
        cv::Vec<_Tp, m * n> resultVec(sourceMatx.val);
        return resultVec;
    }
} }

#endif /* OpencvMatxExtensions_hpp */
