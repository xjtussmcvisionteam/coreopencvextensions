//
//  ScreenCameraInfo.cpp
//  CoreOpenCVExtensions
//
//  Created by JiangZhping on 2017/2/2.
//  Copyright © 2017年 JiangZhping. All rights reserved.
//

#include "../ScreenCameraInfo.h"
#include <sys/utsname.h>
#include <sys/sysctl.h>

std::string ScreenCameraInfo::currentDeviceCode(){
#if TARGET_OS_IPHONE
    struct utsname systemInfo;
    uname(&systemInfo);
    return std::string(systemInfo.machine);
#else
    size_t len = 0;
    sysctlbyname("hw.model", NULL, &len, NULL, 0);
    if (len)
    {
        char *model = (char *) malloc(len*sizeof(char));
        sysctlbyname("hw.model", model, &len, NULL, 0);
        std::string code(model);
        
        auto commaPosition = code.find(",");
        if (commaPosition != std::string::npos) {
            code.replace(code.begin()+commaPosition, code.begin()+commaPosition+1, "_");
        }
        
        free(model);
        return code;
    }
#endif
    return "";
}

