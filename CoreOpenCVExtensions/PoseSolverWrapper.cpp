//
//  PoseSolverWrapper.cpp
//  VADSDelegates
//
//  Created by JiangZhping on 2017/2/1.
//  Copyright © 2017年 JiangZhiping. All rights reserved.
//

#include "PoseSolverWrapper.h"
#include "RPP.h"

std::tuple<cv::Vec3d, cv::Vec3d, cv::Matx33d, cv::Matx44d> PoseSolverWrapper::solvePnP(const std::vector<cv::Point2f> & imagePoints, const std::vector<cv::Point3f> & modelPoints, PoseEstimationSolver solveMethod, const cv::Matx33d & cameraMatrix, const cv::Vec5d distortionEfficient) {
    
    cv::Vec3d rvec, tvec;
    cv::Matx33d pose;
    cv::Matx44d transform;
    if (solveMethod == PnPSolverMethodIterative) {
        cv::solvePnP(modelPoints, imagePoints, cameraMatrix, distortionEfficient, rvec, tvec, false, cv::SOLVEPNP_ITERATIVE);
    } else if(solveMethod == PnPSolverMethodEPnP) {
        cv::solvePnP(modelPoints, imagePoints, cameraMatrix, distortionEfficient, rvec, tvec, false, cv::SOLVEPNP_EPNP);
    } else if(solveMethod == PnPSolverMethodDLS) {
        cv::solvePnP(modelPoints, imagePoints, cameraMatrix, distortionEfficient, rvec, tvec, false, cv::SOLVEPNP_DLS);
    } else if(solveMethod == PnPSolverMethodUPnP) {
        cv::solvePnP(modelPoints, imagePoints, cameraMatrix, distortionEfficient, rvec, tvec, false, cv::SOLVEPNP_UPNP);
    } else if(solveMethod == PnPSolverMethodRPP) {
        cv::Mat modelPointsMat = cv::Mat(modelPoints).reshape(1).t();
        cv::Mat imagePointsMat = cv::Mat(imagePoints).reshape(1).t();
        modelPointsMat.convertTo(modelPointsMat, CV_64F);
        imagePointsMat.convertTo(imagePointsMat, CV_64F);
        cv::Mat1d normalizedImagePointsMat = cv::Mat1d::zeros(3,imagePoints.size());
        normalizedImagePointsMat.row(2).setTo(cv::Scalar(1.0));
        imagePointsMat.copyTo(normalizedImagePointsMat.rowRange(0, 2));
        for (int i = 0 ; i < imagePoints.size(); i ++) {
            cv::Mat1d normalizedP = cv::Mat1d(cameraMatrix.inv()) * normalizedImagePointsMat.col(i);
            normalizedP.copyTo (normalizedImagePointsMat.col(i));
        }
        int iteration;
        double objectError;
        double imageError;
        cv::Mat1d rm;
        cv::Mat1d tvecMat;
        RPP::Rpp(modelPointsMat, normalizedImagePointsMat, rm, tvecMat, iteration, objectError, imageError);
        cv::Rodrigues(rm, rvec);
        tvecMat.copyTo(tvec);
    }
    cv::Rodrigues(rvec, pose);
    transform = cve::matx::joinRotationMatAndTranslationVec(pose, tvec);
    
    return std::make_tuple(rvec, tvec, pose, transform);
}
