//
//  jzplib_camera.h
//  JZP_EYE_TRACKING
//
//  Created by Zhiping Jiang on 14-8-23.
//
//
#ifndef jzplib_camera_h
#define jzplib_camera_h

#include <iostream>
#include <opencv2/opencv.hpp>

namespace cve {
    namespace calibration {
        std::vector<std::vector<cv::Point3f> > calcBoardCornerPositions(int gridW, int gridH, float squareSize, int imagesCount);
        
        bool chessboardCameraCalibration(int gridW, int gridH, float gridSize, const std::vector<std::string> & imagePaths, cv::Mat & cameraMatrix, cv::Mat & distCoeffs, bool drawCorners);
    }
}

#endif /* jzplib_camera_h */
