//
//  jzplib_utilities.cpp
//  BinoCare
//
//  Created by JiangZhping on 2017/3/22.
//
//

#include "jzplib_utilities.h"

namespace cve {
    std::string chronoTime2StringUsingTimeFormat(const std::string & timeFormat, const std::chrono::time_point<std::chrono::system_clock> & chronoTime) {
        auto in_time_t = std::chrono::system_clock::to_time_t(chronoTime);
        std::stringstream ss;
        
        char strChar[512];
        std::strftime(strChar, sizeof(strChar), timeFormat.c_str(), localtime(&in_time_t));
        return std::string(strChar);
    }
}
