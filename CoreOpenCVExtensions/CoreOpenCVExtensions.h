//
//  CoreOpenCVExtensions.h
//  CoreOpenCVExtensions
//
//  Created by JiangZhping on 2016/9/29.
//  Copyright © 2016年 JiangZhping. All rights reserved.
//
#ifndef __JZPLIB_ALL__
#define __JZPLIB_ALL__

#include "AddedOperators.h"
#include "OpencvMatxExtensions.h"
#include "MatxGeometry.h"
#include "CvTypeConversions.h"
#include "jzplib_core.h"
#include "jzplib_camera.h"
#include "jzplib_draw.h"
#include "jzplib_geom.h"
#include "jzplib_image.h"
#include "jzplib_utilities.h"
#include "jzplib_analysis.h"

// PoseSolverWrapper 3
#include "PoseSolverWrapper.h"
#include "RPP.h"
#include "Rpoly.h"

// general utilities
#include "PreferenceManager.h"
#include "TickTockTimer.h"
#include "ScreenCameraInfo.h"

#include "KalmanFilter.h"

#define debugcout(x) std::cout<<#x<<x<<std::endl

#endif
