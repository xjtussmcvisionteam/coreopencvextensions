//
//  jzplib_draw.h
//  OPENCV_HOTSHOTS
//
//  Created by Zhiping Jiang on 14-7-18.
//
//
#ifndef jzplib_draw_h
#define jzplib_draw_h

#include <opencv2/opencv.hpp>

namespace cve {

    /**
     draw a std::vector<cv::Point_<_Tp>> onto the canvas image

     @param canvasMat the canvas image to be drawn on
     @param points a std::vector of cv::Point
     @param color cv::Salar color value
     @param radius dot radius
     @param thickness circle thickness, if -1 fill the circle
     */
    template<typename _Tp> void drawPoints(cv::Mat & canvasMat, const std::vector<cv::Point_<_Tp>> & points, cv::Scalar color=cv::Scalar::all(255), int radius = 1, int thickness = 1);
    
    
    template<typename _Tp> void drawConnectedPoints(cv::Mat& image, const std::vector<cv::Point_<_Tp>> & points, cv::Scalar color= cv::Scalar::all(255), int thickness = 1);
    
    /**
     draw a std::vector<cv::Point_<_Tp>> onto the canvas image with gradient color and radius
     
     @param canvasMat the canvas image to be drawn on
     @param points a std::vector of cv::Point
     @param startColor gradient starting color
     @param endColor gradient ending color
     @param startRadius gradient starting radius
     @param endRadius gradient ending Radius
     @param thickness circle thickness, if -1 fill the circle
     @param pointConnected a bool indicating whether points are connected by line
     @param lineThickness if pointConnected is true, the drawn line thickness
     */
    template<typename _Tp> void drawPointsGradiently(cv::Mat & canvasMat, const std::vector<cv::Point_<_Tp>> & points, cv::Scalar startColor, cv::Scalar endColor, int radius, int endRadius, int thickness, bool pointConnected, int lineThickness);
    
    void drawBox(cv::Mat& image, CvRect box, cv::Scalar color = cvScalarAll(255), int thick=1);
    void drawRotatedRect(cv::Mat image, cv::RotatedRect eyeRRect);
    void drawStringAtPoint(cv::Mat img, const std::string text, cv::Point2i position);
    void drawStringAtTopLeftCorner(cv::Mat img, const std::string text);
}


namespace cve {
    template<typename _Tp> void drawPoints(cv::Mat & canvasMat, const std::vector<cv::Point_<_Tp>> & points, cv::Scalar color, int radius, int thickness) {
        cve::drawPointsGradiently(canvasMat, points, color, color, radius, radius, thickness, false, thickness);
    }
    
    template<typename _Tp> void drawConnectedPoints(cv::Mat & canvasMat, const std::vector<cv::Point_<_Tp>> & points, cv::Scalar color, int thickness) {
        cve::drawPointsGradiently(canvasMat, points, color, color, thickness + 1, thickness + 1, 1, true, thickness);
    }
    
    template<typename _Tp> void drawPointsGradiently(cv::Mat & canvasMat, const std::vector<cv::Point_<_Tp>> & points, cv::Scalar startColor, cv::Scalar endColor, int startRadius, int endRadius, int thickness, bool pointConnected, int lineThickness) {
        for(int i = 0 ; i < points.size(); i ++)
        {
            cv::Scalar currentColor = startColor + (1.0f * i / points.size()) * (endColor - startColor);
            int currentRadius = startRadius + (1.0f * i / points.size()) * (endRadius - startRadius);
            cv::circle(canvasMat, points[i], currentRadius, currentColor, thickness);
            
            if (pointConnected && i > 0) {
                cv::line(canvasMat, points[i-1], points[i], currentColor, lineThickness, CV_AA);
            }
        }
    }
}

#endif
