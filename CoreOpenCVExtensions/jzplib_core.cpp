//
//  jzplib_core.cpp
//  CoreOpenCVExtensions
//
//  Created by JiangZhping on 2016/12/30.
//  Copyright © 2016年 JiangZhping. All rights reserved.
//

#include "jzplib_core.h"

namespace cve {
    cv::Mat cropROIWithBoundaryDetection(const cv::Mat & sourceImage, const cv::Rect2i & roi) {
        cv::Rect2i intRoi = roi;
        cv::Rect2i imageRect = cv::Rect2i(0,0,sourceImage.cols, sourceImage.rows);
        cv::Rect2i intersection = imageRect & intRoi;
        return sourceImage(intersection).clone();
    }
    
    cv::Mat1d normr(const cv::Mat & input) {
        cv::Mat1d result = cv::Mat1d::zeros(input.rows, 1);
        for (int i = 0; i < input.rows; i ++) {
            result(i) = cv::norm(input.row(i));
        }
        return result;
    }
    
    cv::Mat1d normc(const cv::Mat & input) {
        cv::Mat1d result = cv::Mat1d::zeros(1, input.rows);
        for (int i = 0; i < input.cols; i ++) {
            result(i) = cv::norm(input.col(i));
        }
        return result;
    }
    
    cv::Mat submatrixByRowIndex(const cv::Mat & sourceMat, const cv::Mat & index) {
        cv::Mat returnMat; sourceMat.copyTo(returnMat);
        for (int i = 0 ; i < index.total(); i ++) {
            sourceMat.row(index.at<int>(i)).copyTo(returnMat.row(i));
        }
        returnMat.rowRange(0, (int)index.total()).copyTo(returnMat);
        return returnMat;
    }
}
