#pragma once
#include "plot.h"
#include "jzplib_image.h"
#include <opencv2/opencv.hpp>  
#include <map>
#include <iostream>

using namespace std;
using namespace cv;
using namespace cve;



class PlotManager
{
private:
	map<string, Plot> plots;
	map<string, vector<vector<Point2d>>> pointdata;
	map<string, vector<Scalar>> colordata;

	void draw(string name) {
		if (pointdata[name].size() > 0) {
			int linecount = pointdata[name][0].size();
			vector<vector<Point2d>> lines(linecount);
			for (vector<Point2d> var : pointdata[name]) {
				int i = 0;
				for (Point2d point : var) {
					lines[i].push_back(point);
					i++;
				}
			}
			int j = 0;
			plots[name].clear();
			for (vector<Point2d> var : lines) {
				plots[name].plot(var, colordata[name][j]);
				j++;
			}
			plots[name].figure();
		}
		else {
			plots[name].figure();
		}
	}
public:
	PlotManager() {};

	void addPoint(string name, vector<double> points, vector<Scalar> colors = {}) {
		if (plots.find(name) == plots.end()) {
			plots[name];
			vector<Point2d> floatPoints(0);
			for (double var : points) {
				floatPoints.push_back(Point2d(pointdata[name].size(), var));
			}
			pointdata[name].push_back(floatPoints);
			if (colors.size()>0) {
				if (colors.size() != pointdata[name][0].size()) {
					cout << name << "'s colors num mismatch!" << endl;
					return;
				}
				colordata[name] = colors;
			}
			else {
				Mat1b baseMat(pointdata[name][0].size(), 1);
				for (char i = 0; i < pointdata[name][0].size(); i++) {
					baseMat.at<char>(i, 0) = float(i) / float(pointdata[name][0].size()) * 255;
				}
				Mat3b colors = imagesc(baseMat);
				vector<Scalar> defaultcolor(pointdata[name][0].size());
				for (int j = 0; j < pointdata[name][0].size(); j++) {
					defaultcolor[j] = colors.at<Vec3b>(j, 0);
				}
				colordata[name] = defaultcolor;
			}
			return;
		}
		if (pointdata[name][0].size() != points.size()) {
			cout << name << "'s point num mismatch!" << endl;
			return;
		}
		if (colors.size()>0) {
			if (colors.size() != pointdata[name][0].size()) {
				cout << name << "'s colors num mismatch!" << endl;
				return;
			}
			colordata[name] = colors;
		}
		vector<Point2d> floatPoints(0);
		for (double var : points) {
			floatPoints.push_back(Point2d(pointdata[name].size(), var));
		}
		pointdata[name].push_back(floatPoints);
	}

	void addPoint(string name, vector<Point2d> points, vector<Scalar> colors = {}) {
		if (plots.find(name) == plots.end()) {
			plots[name];
			pointdata[name].push_back(points);
			if (colors.size()>0) {
				if (colors.size() != pointdata[name][0].size()) {
					cout << name << "'s colors num mismatch!" << endl;
					return;
				}
				colordata[name] = colors;
			}
			else {
				Mat1b baseMat(pointdata[name][0].size(), 1);
				for (char i = 0; i < pointdata[name][0].size(); i++) {
					baseMat.at<char>(i, 0) = float(i) / float(pointdata[name][0].size()) * 255;
				}
				Mat3b colors = imagesc(baseMat);
				vector<Scalar> defaultcolor(pointdata[name][0].size());
				for (int j = 0; j < pointdata[name][0].size(); j++) {
					defaultcolor[j] = colors.at<Vec3b>(j, 0);
				}
				colordata[name] = defaultcolor;
			}
			return;
		}
		if (pointdata[name][0].size() != points.size()) {
			cout << name << "'s point num mismatch!" << endl;
			return;
		}
		if (colors.size()>0) {
			if (colors.size() != pointdata[name][0].size()) {
				cout << name << "'s colors num mismatch!" << endl;
				return;
			}
			colordata[name] = colors;
		}
		pointdata[name].push_back(points);
	}

	Mat getFigure(string name) {
		draw(name);
		return plots[name].figure();
	}

	void clearFigure(string name) {
		plots[name].clear();
		pointdata[name].clear();
		colordata[name].clear();
		plots.erase(name);
		pointdata.erase(name);
		colordata.erase(name);
	}

};