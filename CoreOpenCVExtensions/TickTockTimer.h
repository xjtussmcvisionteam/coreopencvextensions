//
//  jzplib_utilities.hpp
//  CoreOpenCVExtensions
//
//  Created by JiangZhping on 2017/1/30.
//  Copyright © 2017年 JiangZhping. All rights reserved.
//

#ifndef jzplib_utilities_hpp
#define jzplib_utilities_hpp

#include <iostream>
#include <chrono>
#include <numeric>
#include <deque>

using namespace std::chrono;

namespace cve {
    class TickTockTimer {
    private:
        std::string timerName;
        std::deque<double> durations;
        int durationLength = 50;
        time_point<system_clock> startTime;
    public:
        TickTockTimer(const std::string & timerName, int timerLength = 50): timerName{timerName}, durationLength{timerLength} {}
        
        void tick();
        void tock();
    };
}


#endif /* jzplib_utilities_hpp */
