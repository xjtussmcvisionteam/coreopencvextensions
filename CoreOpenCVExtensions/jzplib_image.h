//
//  jzplib_image.hpp
//  CoreOpenCVExtensions
//
//  Created by JiangZhping on 2016/12/29.
//  Copyright © 2016年 JiangZhping. All rights reserved.
//

#ifndef jzplib_image_hpp
#define jzplib_image_hpp

#include <opencv2/opencv.hpp>

namespace cve {
    
    /**
     use copyTo method to apply mask onto the input image. (the output image of copyTo method cannot be the input itself, so to avoid it).

     @param imageTobeMasked image to be masked.
     @param mask CV_8U mask
     */
    void applyMask(cv::Mat & imageTobeMasked, const cv::Mat & mask);
    
    /**
     A mimic of Matlab imresize function

     @param original the image to be resized
     @param scale the scaling factor
     @param interpolation resize interpolation protocol, default is cv::INTER_LINEAR
     @return resized image
     */
    cv::Mat imresize(const cv::Mat & original, float scale = 1.0f, cv::InterpolationFlags interpolation = cv::INTER_LINEAR);
    
    /**
     A mimic of Matlab imagesc function.

     @param original A 1-Channel gray or float image.
     @param colorMap the Colormap for rendering
     @return A color image of type Mat3b
     */
    cv::Mat3b imagesc(const cv::Mat & original, cv::ColormapTypes colorMap = cv::COLORMAP_JET);
    
    
    cv::Mat luminanceBalancedEqualHist(const cv::Mat & original);
    
    /**
     Identify the local maxima(s) in the given image.

     @param input A 1-channel gray image or float image.
     @param nLocMax The maximum number of local maxima to be identified.
     @param minDistBtwLocMax The minimum pixel distance between two local maximas.
     @param threshold The threshold value of local maxima. If negative(< -1), the threshold = minValue(input).
     @return A vector of pixel coordinate(s) (in Pixel2i type) of the identified local maxima(s).
     */
    std::tuple<std::vector<cv::Point2i>, std::vector<double>> imageLocalMaxima(const cv::Mat & input, int nLocMax = 1, float minDistBtwLocMax = 1, float threshold = -1, cv::InputArray mask = cv::noArray());
    
    /**
     A mimic of Matlab imgraydient + imgraydientxy

     @param grayImage the given gray or bw image
     @return return(0) is the magnitude of gradient, return(1) is a 2-channel 32F mat. The first layer is GradientX, the second is GradientY.
     */
    std::tuple<cv::Mat1f, cv::Mat2f> imgradient(const cv::Mat & grayOrBwImage);

    /**
     Extended version of cv::filter2d. It takes a cv::Rect, and perform filter2D only the image rect, so to improve performance. The output image size can be the same as the source image, or only the rect.

     @param imageRect The rect used to crop the sourceImage
     @param outputSameSizeAsSourceImage If true, the destImage.size() = sourceImage.size() and the border is filled with zeros.

     */
    void filter2DForImageRect(cv::InputArray sourceImage, cv::Rect imageRect, cv::OutputArray destImage, int ddepth, cv::InputArray kernel, bool outputSameSizeAsSourceImage = true, cv::Point anchor = cv::Point(-1,-1), double delta = 0, int borderType = cv::BORDER_TRANSPARENT);
}

#endif /* jzplib_image_hpp */
