//
//  jzplib_analysis.hpp
//  CoreOpenCVExtensions
//
//  Created by JiangZhping on 2017/1/3.
//  Copyright © 2017年 JiangZhping. All rights reserved.
//

#ifndef jzplib_analysis_h
#define jzplib_analysis_h

#include <opencv2/opencv.hpp>

namespace cve {
    template<typename _Tp> cv::Mat1b contourPointsToMaskViaConvexHull(const std::vector<cv::Point_<_Tp>> & points, const cv::Size & maskSize);
    
    /**
     remove the small blobs (or noise dots) and leaving only the largest blob

     @param bwImage binary image to be processed
     @return a binary image containing only the largest blob
     */
    cv::Mat removeSmallBlobsExceptLargest(const cv::Mat & bwImage);
    
    /**
     compuate PCA for the point set, and return Mean point and orthogonal axes.

     @param pointMat point set Mat, it could be Mat(std::vector<Vec<float, dimension>) matrix, a N x dimension matrix, or a Mat(std::vector<cv::Point(or 3)>) matrix
     @param dimension the dimension of returned Vec.
     @return A std::vector whose 1st element is the mean, and the rest are orthogonal axes
     */
    template<typename _Tp = float, int dimension> std::vector<cv::Vec<_Tp, dimension>> computePCACenterAndAxis(const cv::Mat & pointMat);
    
    /**
     generate the (dimension + 1) x (dimension +1) size [R | T; 0 1] transformation matrix

     @param pcaResult the PCA comptuation result from method @sa computePCACenterAndAxis
     @return A fixed-size and type Matx transofrmation matrix
     */
    template<typename _Tp, int dimension> cv::Matx<_Tp, dimension + 1, dimension +1> generateTransformMatrixFromPCAResult(std::vector<cv::Vec<_Tp, dimension>> pcaResult);
    
    /**
     Smart color image segmentation using iterative k-means clustering. This method will perform multiple binary k-means clustering on the "darker" part, so that the darkest part can be clearly seperated (e.g iris area).

     @param image the image to be segmented
     @param kmeansIterations level of segmentation. Default is 4.
     @param coordinateWeight the weight factor for pixel adjacency. 0 will perform clustering purely be color, and higher coordinateWeight will weight more on pixel adjacency. Default is 0.4f
     @param kmeansRepeats times of k-means repeating for each level. Default is 4.
     @return a ranked image. higher value mean darker.
     */
    cv::Mat1b imageSegmentationViaIterativeKmeans(const cv::Mat& image, int kmeansIterations = 4, float coordinateWeight = 0.4f, int kmeansRepeats = 4);
    
    /**
     Fill the convex hulls in binary image.

     @return concavity-filled binary image
     */
    cv::Mat fillConvexHulls(const cv::Mat & bwImage);
    
    /**
     return the mass center coordinates for the blobs in the given binary image.
     */
    std::vector<cv::Point2f> blobMassCenter(const cv::Mat & bwImage);
}

namespace cve {
    template<typename _Tp> cv::Mat1b contourPointsToMaskViaConvexHull(const std::vector<cv::Point_<_Tp>> & points, const cv::Size & maskSize) {
        cv::Mat pointMat = cv::Mat(points);
        pointMat.convertTo(pointMat, CV_32S);
        std::vector<std::vector<cv::Point>> hulls(1);
        cv::convexHull(pointMat, hulls[0]);
        cv::Mat1b mask = cv::Mat1b::zeros(maskSize);
        cv::drawContours(mask, hulls, -1, cv::Scalar(255), CV_FILLED);
        
        return mask;
    }
    
    template<typename _Tp, int dimension> std::vector<cv::Vec<_Tp, dimension>> computePCACenterAndAxis(const cv::Mat & pointMat) {
        
        int cvTypeId = 0;
        if (std::is_same<_Tp, float>()) {
            cvTypeId = CV_32F;
        }
        if (std::is_same<_Tp, double>()) {
            cvTypeId = CV_64F;
        }
        
        cv::Mat ptsMat = cv::Mat(pointMat).reshape(1,pointMat.rows);
        cv::PCA pca_analysis(ptsMat, cv::Mat(), CV_PCA_DATA_AS_ROW);
        cv::Mat_<_Tp> meanMat;
        pca_analysis.mean.convertTo(meanMat, cvTypeId);
        _Tp meanValues[dimension];
        for (int i = 0 ; i < dimension; i++) {
            meanValues[i] = meanMat.template at<_Tp>(i);
        }
        cv::Vec<_Tp, dimension> center(meanValues);
        std::vector<cv::Vec<_Tp, dimension>> returnVector(dimension + 1);
        returnVector[0] = center;
        
        cv::Mat_<_Tp> floatEigenVectors; pca_analysis.eigenvectors.convertTo(floatEigenVectors, cvTypeId);
        for (int i = 0; i < dimension ; i ++) {
            _Tp axisValues[dimension];
            for (int j = 0 ; j < dimension; j++) {
                axisValues[j] = floatEigenVectors.template at<_Tp>(i,j);
            }
            cv::Vec<_Tp, dimension> axis(axisValues);
            returnVector[i + 1] = axis;
        }
        
        return returnVector;
    }
    
    template<typename _Tp, int dimension> cv::Matx<_Tp, dimension + 1, dimension +1> generateTransformMatrixFromPCAResult(std::vector<cv::Vec<_Tp, dimension>> pcaResult) {
        cv::Matx<_Tp, dimension + 1, dimension +1> transform = cv::Matx<_Tp, dimension + 1, dimension +1>::eye();
        
        // set the translation
        for(int i = 0 ; i < dimension; i ++) {
            transform(i, dimension) = pcaResult[0](i);
        }
        
        // set the rotation
        for (int i = 0 ; i < dimension ; i++) {
            for (int j = 0 ; j < dimension; j ++ ) {
                transform(i, j) = pcaResult[i + 1](j);
            }
        }
        
        return transform;
    }
}

#endif /* jzplib_analysis_h */
