//
//  PoseSolverWrapper.hpp
//  VADSDelegates
//
//  Created by JiangZhping on 2017/2/1.
//  Copyright © 2017年 JiangZhiping. All rights reserved.
//

#ifndef PoseSolverWrapper_hpp
#define PoseSolverWrapper_hpp

#include "OpencvMatxExtensions.h"
#include "MatxGeometry.h"

enum PoseEstimationSolver {
    PnPSolverMethodIterative = 0,
    PnPSolverMethodEPnP = 1,
    PnPSolverMethodDLS = 2,
    PnPSolverMethodUPnP = 3,
    PnPSolverMethodRPP = 4,
};

class PoseSolverWrapper {
public:
    static std::tuple<cv::Vec3d, cv::Vec3d, cv::Matx33d, cv::Matx44d> solvePnP(const std::vector<cv::Point2f> & imagePoints, const std::vector<cv::Point3f> & modelPoints, PoseEstimationSolver solveMethod, const cv::Matx33d & cameraMatrix, const cv::Vec5d distortionEfficient);
};


#endif /* PoseSolverWrapper_hpp */
