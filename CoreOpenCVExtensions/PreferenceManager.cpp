//
//  PreferenceManager.cpp
//  CoreOpenCVExtensions
//
//  Created by JiangZhping on 2017/1/30.
//  Copyright © 2017年 JiangZhping. All rights reserved.
//

#include "PreferenceManager.h"

std::string PreferenceManager::storageFilePath;
bool PreferenceManager::instantFlush;
// an empty yaml document
std::string PreferenceManager::internalDataString;


static std::string readFileIntoString(const std::string & filepath) {
    std::ifstream in(filepath);
    std::string contents((std::istreambuf_iterator<char>(in)),
                         std::istreambuf_iterator<char>());
    return contents;
}

PreferenceManager::PreferenceManager() {
    storageFilePath = "preference.yaml";
    instantFlush = false;
    if (std::ifstream(storageFilePath)) {
        internalDataString = readFileIntoString(storageFilePath);
    } else {
        internalDataString = "%YAML:1.0\n---\n";
    }
}

PreferenceManager::~PreferenceManager() {
    this->flushToDisk();
}

bool PreferenceManager::keyExists(const std::string &stringId) {
    cv::FileStorage storageReader(internalDataString, cv::FileStorage::READ + cv::FileStorage::MEMORY);
    bool result =  !storageReader[stringId].isNone();
    storageReader.release();
    return result;
}

void PreferenceManager::flushToDisk() {
    std::ofstream file(storageFilePath);
    file << internalDataString;
    file.close();
}
