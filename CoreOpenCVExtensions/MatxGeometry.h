//
//  MatxGeometry.hpp
//  CoreOpenCVExtensions
//
//  Created by JiangZhping on 2017/1/18.
//  Copyright © 2017年 JiangZhping. All rights reserved.
//

#ifndef MatxGeometry_hpp
#define MatxGeometry_hpp

#include <iostream>
#include <opencv2/opencv.hpp>
#include "AddedOperators.h"

namespace cve { namespace matx {
    
    /**
     Make rotation matrix from joint z-y-x euler angle rotation.

     @param xRadian x-axis rotation radian
     @param yRadian y-axis rotation radian
     @param zRadian z-axis rotation radian
     @return Matx(_Tp, 3, 3) rotation matrix.
     */
    template<typename _Tp> cv::Matx<_Tp, 3, 3> rotationMatrixFromEulerAngles(_Tp xRadian, _Tp yRadian, _Tp zRadian);
    
    
    template<typename _Tp, int dimension> cv::Matx<_Tp, dimension+1, dimension+1> transformFromTranslation(const cv::Matx<_Tp, dimension, 1> & translation);
    
    /**
     Generate a transformation matrix (d+1, d+1) by joining a rotation matrix (d,d) and a translation vector (d,1)
     
     @return the returned matx is of (d+1, d+1) dimension and with the same type of rotation and translation.
     */
    template<int dimension, typename _Tp> cv::Matx<_Tp, dimension+1, dimension+1> joinRotationMatAndTranslationVec(const cv::Matx<_Tp, dimension, dimension> & rotation, const cv::Matx<_Tp, dimension, 1> & translation);
    
    /**
     perform perspective transform upon a given vector
     
     @param transform a (d+1, d+1) size transform matrix with type _Tp
     @param vector a (d,1) vector to be transformed with type _Tp
     @return the (d,1) transfomed vector with the same type of input vector
     */
    template<int dimension, typename _Tp> cv::Vec<_Tp, dimension> transformVectorWithTransformMat(const cv::Matx<_Tp, dimension +1, dimension +1> & transform, const cv::Matx<_Tp, dimension, 1> & vector);

} }

#pragma mark - Implementation
namespace cve { namespace matx {
    
    template<typename _Tp> cv::Matx<_Tp, 3, 3> rotationMatrixFromEulerAngles(_Tp xRadian, _Tp yRadian, _Tp zRadian) {
        _Tp cosx = cos(xRadian);
        _Tp sinx = sin(xRadian);
        _Tp cosy = cos(yRadian);
        _Tp siny = sin(yRadian);
        _Tp cosz = cos(zRadian);
        _Tp sinz = sin(zRadian);
        
        cv::Matx<_Tp, 3, 3> xRotation(1.0, 0.0, 0.0, 0.0, cosx, - sinx, 0, sinx, cosx);
        cv::Matx<_Tp, 3, 3> yRotation(cosy, 0.0, siny, 0.0, 1.0, 0, siny, 0, cosy);
        cv::Matx<_Tp, 3, 3> zRotation(cosz, sinz, 0.0, -sinz, cosz, 0.0, 0.0, 0.0, 1.0);
        
        return zRotation * yRotation * xRotation;
    }
    
    
    template<typename _Tp, int dimension> cv::Matx<_Tp, dimension+1, dimension+1> transformFromTranslation(const cv::Matx<_Tp, dimension, 1> & translation) {
        cv::Matx<_Tp, dimension+1, dimension+1> resultMat = cv::Matx<_Tp, dimension+1, dimension+1>::eye();
        for (int i = 0 ; i < dimension; i ++) {
            resultMat(i,dimension) = translation(i);
        }
        return resultMat;
    }
    
    template<int dimension, typename _Tp> cv::Matx<_Tp, dimension+1, dimension+1> joinRotationMatAndTranslationVec(const cv::Matx<_Tp, dimension, dimension> & rotation, const cv::Matx<_Tp, dimension, 1> & translation) {
        cv::Matx<_Tp, dimension+1, dimension+1> resultMat;
        for (int i = 0 ; i < dimension; i ++) {
            for (int j = 0 ; j < dimension; j ++) {
                resultMat(i,j) = rotation(i,j);
            }
        }
        for (int i = 0 ; i < dimension; i ++) {
            resultMat(i,dimension) = translation(i);
        }
        
        for (int i = 0 ; i < dimension; i ++) {
            resultMat(dimension,i) = 0;
        }
        resultMat(dimension,dimension) = 1;
        
        return resultMat;
    }
    
    template<int dimension, typename _Tp> cv::Vec<_Tp, dimension> transformVectorWithTransformMat(const cv::Matx<_Tp, dimension +1, dimension +1> & transform, const cv::Matx<_Tp, dimension, 1> & vector) {
        _Tp homoVals[dimension +1];
        memcpy(homoVals, vector.val, dimension * sizeof(_Tp));
        homoVals[dimension] = (_Tp)1.0;
        cv::Vec<_Tp, dimension+1> homoV(homoVals);
        cv::Vec<_Tp, dimension+1> homoResult = transform * homoV;
        return cv::Vec<_Tp, dimension>(homoResult.val);
    }
} }

#endif /* MatxGeometry_hpp */
