//
//  jzplib_utilities.hpp
//  BinoCare
//
//  Created by JiangZhping on 2017/3/22.
//
//

#ifndef jzplib_utilities_h
#define jzplib_utilities_h

#include <vector>
#include <string>
#include <fstream>
#include <iomanip>
#include <chrono>
#include <ctime>
#include <sstream>

namespace cve {
    std::string chronoTime2StringUsingTimeFormat(const std::string & timeFormat = "%Y%m%d_%H%M", const std::chrono::time_point<std::chrono::system_clock> & chronoTime = std::chrono::system_clock::now());
    
    template<typename _Tp> class CSVFileWriter {
    public:
        std::string delimeter = " ";
        void addCSVLine(std::vector<_Tp> slot) {
            slots.push_back(slot);
        }
        void writeToFile(const std::string & filePath) {
            
            std::ofstream textFile(filePath);
            
            int i = 0, j =0 ;
            for (i= 0; i< slots.size(); i++) {
                for (j = 0; j < slots[i].size()-1; j++) {
                    textFile<<std::setprecision(10) <<slots[i][j]<<delimeter;
                }
                textFile<<std::setprecision(10) <<slots[i][j]<<std::endl;
            }
            textFile.flush();
            textFile.close();
        };
    private:
        std::vector<std::vector<_Tp>> slots;
    };
}

#endif /* jzplib_utilities_h */
