//
//  jzplib_geom.cpp
//  OPENCV_HOTSHOTS
//
//  Created by Zhiping Jiang on 14-7-18.
//
//

#include "jzplib_geom.h"


namespace cve {
    
    cv::Mat cropRotatedRectFromImage(const cv::RotatedRect & rotatedRect, const cv::Mat & image) {
        cv::Rect2f boundingRect = rotatedRect.boundingRect();
        
        boundingRect.x = boundingRect.x >= 0 ? boundingRect.x : 0;
        boundingRect.y = boundingRect.y >= 0 ? boundingRect.y : 0;
        boundingRect.width = boundingRect.x+boundingRect.width < image.size().width ? boundingRect.width : image.size().width - boundingRect.x;
        boundingRect.height = boundingRect.y+boundingRect.height < image.size().height ? boundingRect.height : image.size().height - boundingRect.y;
        
        cv::Point2f centerInRect = cv::Point2f(boundingRect.size().width, boundingRect.size().height) * 0.5;
        cv::Mat1d localRotationM = getRotationMatrix2D(centerInRect, rotatedRect.angle, 1.0);
        cv::Mat rotatedBox, croppedImage;
        warpAffine(image(boundingRect), rotatedBox, localRotationM, boundingRect.size());
        getRectSubPix(rotatedBox, rotatedRect.size, centerInRect, croppedImage);
        return croppedImage;
    }
    
    cv::Point2f pointInRotatedRect(const cv::RotatedRect & rotatedRect, const cv::Point2f & originalPoint) {
        cv::Mat1d localRotationM = getRotationMatrix2D(rotatedRect.center, rotatedRect.angle, 1.0);
        cv::Point2f centerInRRect = cv::Point2f(rotatedRect.size.width, rotatedRect.size.height) * 0.5;
        localRotationM(0,2) = centerInRRect.x;
        localRotationM(1,2) = centerInRRect.y;
        cv::Mat2d resultPoint;
        cv::transform(cv::Mat2d(originalPoint - rotatedRect.center), resultPoint, localRotationM);
        
        double x = resultPoint(0)[0] < 0.0 ? 0 : resultPoint(0)[0];
        double y = resultPoint(0)[1] < 0.0 ? 0 : resultPoint(0)[1];
        x = x > rotatedRect.size.width ? rotatedRect.size.width : x;
        y = y > rotatedRect.size.height ? rotatedRect.size.height : y;
        
        return cv::Point2f(x, y);
    }
    
    cv::Point2f invertPointInRotatedRect(const cv::Point2f & point, const cv::RotatedRect &rotatedRect) {
        cv::Point2f centerInRRect = cv::Point2f(rotatedRect.size.width, rotatedRect.size.height) * 0.5;
        cv::Point2f pointCentered = point - centerInRRect;
        
        cv::Mat1d invertT = getRotationMatrix2D(cv::Point2f(0,0), -rotatedRect.angle, 1.0);
        invertT(0,2) = (double)rotatedRect.center.x;
        invertT(1,2) = (double)rotatedRect.center.y;
        cv::Mat2d resultMat;
        cv::transform(cv::Mat2d(pointCentered), resultMat, invertT);
        return cv::Point2f(resultMat(0)[0], resultMat(0)[1]);
    }
    
    cv::Vec3d rotationVector2EulerAngles(const cv::Vec3d & rvec) {
        cv::Matx33d rotationMatrix;
        cv::Rodrigues(rvec, rotationMatrix);
        return rotationMatrix2EulerAngles(rotationMatrix);
    }
    
    cv::Vec3d rotationMatrix2EulerAngles(const cv::Matx33d & rotationMatrix)
    {
        cv::Vec3d euler;
        
        double m00 = rotationMatrix(0,0);
        double m10 = rotationMatrix(1,0);
        double m11 = rotationMatrix(1,1);
        double m12 = rotationMatrix(1,2);
        double m20 = rotationMatrix(2,0);
        
        double x, y, z;
        
        // Assuming the angles are in radians.
        //    if (m10 > 0.998) { // singularity at north pole
        //        x = 0;
        //        y = CV_PI/2;
        //        z = atan2(m02,m22);
        //    }
        //    else if (m10 < -0.998) { // singularity at south pole
        //        x = 0;
        //        y = -CV_PI/2;
        //        z = atan2(m02,m22);
        //    }
        //    else
        //    {
        x = atan2(-m12,m11);
        y = asin(m10);
        z = atan2(-m20,m00);
        //    }
        
        euler(0) = x;
        euler(1) = y;
        euler(2) = z;
        
        return euler;
    }
    
    // Converts a given Euler angles to Rotation Matrix
    cv::Matx33d eulerAngles2RotationMatrix (const cv::Vec3d & euler)
    {
        cv::Matx33d rotationMatrix;
        
        double x = euler(0);
        double y = euler(1);
        double z = euler(2);
        
        // Assuming the angles are in radians.
        double ch = cos(z);
        double sh = sin(z);
        double ca = cos(y);
        double sa = sin(y);
        double cb = cos(x);
        double sb = sin(x);
        
        double m00, m01, m02, m10, m11, m12, m20, m21, m22;
        
        m00 = ch * ca;
        m01 = sh*sb - ch*sa*cb;
        m02 = ch*sa*sb + sh*cb;
        m10 = sa;
        m11 = ca*cb;
        m12 = -ca*sb;
        m20 = -sh*ca;
        m21 = sh*sa*cb + ch*sb;
        m22 = -sh*sa*sb + ch*cb;
        
        rotationMatrix(0,0) = m00;
        rotationMatrix(0,1) = m01;
        rotationMatrix(0,2) = m02;
        rotationMatrix(1,0) = m10;
        rotationMatrix(1,1) = m11;
        rotationMatrix(1,2) = m12;
        rotationMatrix(2,0) = m20;
        rotationMatrix(2,1) = m21;
        rotationMatrix(2,2) = m22;
        
        return rotationMatrix;
    }
}


