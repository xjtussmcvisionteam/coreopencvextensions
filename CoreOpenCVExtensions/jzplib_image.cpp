//
//  jzplib_image.cpp
//  CoreOpenCVExtensions
//
//  Created by JiangZhping on 2016/12/29.
//  Copyright © 2016年 JiangZhping. All rights reserved.
//

#include "jzplib_image.h"

namespace cve {
    
    void applyMask(cv::Mat & imageToBeMasked, const cv::Mat & mask) {
        cv::Mat temp;
        imageToBeMasked.copyTo(temp, mask);
        temp.copyTo(imageToBeMasked);
    }
    
    cv::Mat imresize(const cv::Mat & original, float scale, cv::InterpolationFlags interpolation) {
        cv::Mat dst;
        cv::resize(original, dst, cv::Size(), scale, scale, interpolation);
        return dst;
    }
    
    cv::Mat3b imagesc(const cv::Mat & original, cv::ColormapTypes colorMap) {
        cv::Mat1b grayColorImage;
        cv::Mat3b falseColorsImage;
        cv::normalize(original, grayColorImage, 255.0, 0.0, cv::NORM_MINMAX, CV_8U);
        cv::applyColorMap(grayColorImage, falseColorsImage, colorMap);
        
        return falseColorsImage;
    }
    
    cv::Mat luminanceBalancedEqualHist(const cv::Mat & original) {
        cv::Mat gray_img = original.clone();
        int w = gray_img.cols;
        int h = gray_img.rows;
        cv::Mat wholeFace;
        equalizeHist(gray_img, wholeFace); int midX = w/2;
        cv::Mat leftSide = gray_img(cv::Rect2i(0,0, midX,h));
        cv::Mat rightSide = gray_img(cv::Rect2i(midX,0, w-midX,h));
        equalizeHist(leftSide, leftSide);
        equalizeHist(rightSide, rightSide);
        
        for (int y=0; y<h; y++) {
            for (int x=0; x<w; x++) {
                int v;
                if (x < w/4) {
                    // Left 25%: just use the left face.
                    v = leftSide.at<uchar>(y,x);
                }
                else if (x < w*2/4) {
                    // Mid-left 25%: blend the left face & whole face.
                    int lv = leftSide.at<uchar>(y,x);
                    int wv = wholeFace.at<uchar>(y,x);
                    // Blend more of the whole face as it moves
                    // further right along the face.
                    float f = (x - w*1/4) / (float)(w/4);
                    v = cvRound((1.0f - f) * lv + (f) * wv);
                }
                else if (x < w*3/4) {
                    // Mid-right 25%: blend right face & whole face.
                    int rv = rightSide.at<uchar>(y,x-midX);
                    int wv = wholeFace.at<uchar>(y,x);
                    // Blend more of the right-side face as it moves
                    // further right along the face.
                    float f = (x - w*2/4) / (float)(w/4);
                    v = cvRound((1.0f - f) * wv + (f) * rv);
                }
                else {
                    // Right 25%: just use the right face.
                    v = rightSide.at<uchar>(y,x-midX);
                }
                gray_img.at<uchar>(y,x) = v;
            }// end x loop
        }//end y loop
        
        
        return gray_img;
    }
    
    std::tuple<std::vector<cv::Point2i>, std::vector<double>> imageLocalMaxima(const cv::Mat& input, int nLocMax, float minDistBtwLocMax, float threshold, cv::InputArray mask) {
        cv::Mat1f playground;
        input.convertTo(playground, CV_32FC1);
        std::vector<cv::Point2i> peakPoints;
        std::vector<double> maxValues;
        
        if (threshold == -1) {
            double temp = 0;
            cv::minMaxLoc(playground, &temp, NULL, NULL, NULL, mask);
            threshold = temp;
        }
        for (int i = 0 ; i < nLocMax; i++) {
            cv::Point2i location;
            double maxVal;
            cv::minMaxLoc(playground, NULL, &maxVal, NULL, &location, mask);
            if (maxVal >= threshold) {
                peakPoints.push_back(location);
                maxValues.push_back(maxVal);
                cv::circle(playground, location, minDistBtwLocMax, cv::Scalar::all(threshold), -1);
            } else
                break;
        }
        
        return std::make_tuple(peakPoints, maxValues);
    }
    
    std::tuple<cv::Mat1f, cv::Mat2f> imgradient(const cv::Mat & grayOrBwImage) {
        cv::Mat1f grad_x, grad_y;
        cv::Scharr(grayOrBwImage, grad_x, CV_32F, 1, 0);
        cv::Scharr(grayOrBwImage, grad_y, CV_32F, 0, 1);
        cv::Mat1f magnitude = grad_x.mul(grad_x) + grad_y.mul(grad_y);
        cv::sqrt(magnitude, magnitude);
        grad_x /= magnitude;
        grad_y /= magnitude;
        
        cv::Mat2f gradientDir;
        cv::merge(std::vector<cv::Mat1f> {grad_x, grad_y}, gradientDir);
        return std::make_tuple(magnitude, gradientDir);
    }

    void filter2DForImageRect(cv::InputArray sourceImage, cv::Rect imageRect, cv::OutputArray destImage, int ddepth, cv::InputArray kernel, bool outputSameSizeAsSourceImage, cv::Point anchor, double delta, int borderType) {
        
        cv::Mat kernelMat = kernel.getMat();
        cv::Mat sourceImageMat = sourceImage.getMat();
        
        // TODO add some CV_Assert
        
        int left = std::min(imageRect.x, kernelMat.size().width / 2);
        int right = std::min(sourceImageMat.size().width - imageRect.x - imageRect.width, kernelMat.size().width / 2);
        int top = std::min(imageRect.y, kernelMat.size().height / 2);
        int bottom = std::min(sourceImageMat.size().height - imageRect.y - imageRect.height, kernelMat.size().height / 2);
        
        cv::Rect operationRect = cv::Rect(imageRect.x - left, imageRect.y - top, imageRect.width + left + right, imageRect.height + top + bottom);
        cv::Mat sourceImageROI = sourceImageMat(imageRect);
        cv::Mat borderRefilledROIImage;
        copyMakeBorder(sourceImageROI, borderRefilledROIImage, top, bottom, left, right, borderType);
        cv::Mat convolutionResult;
        cv::filter2D(borderRefilledROIImage, convolutionResult, ddepth, kernel);
        
        if (outputSameSizeAsSourceImage) {
            cv::Mat result = cv::Mat::zeros(sourceImageMat.size(), convolutionResult.type());
            convolutionResult.copyTo(result(operationRect));
            result.copyTo(destImage);
        } else {
            convolutionResult.copyTo(destImage);
        }
        
    }
}

