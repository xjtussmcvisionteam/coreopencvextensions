//
//  jzplib_utilities.cpp
//  CoreOpenCVExtensions
//
//  Created by JiangZhping on 2017/1/30.
//  Copyright © 2017年 JiangZhping. All rights reserved.
//

#include "TickTockTimer.h"

namespace cve {
    void TickTockTimer::tick() {
        startTime = system_clock::now();
    }
    
    void TickTockTimer::tock() {
        time_point<system_clock> endTime = system_clock::now();
        duration<double> elipsed_seconds = endTime - startTime;
        durations.push_back(elipsed_seconds.count());
        
        if (durations.size() > durationLength) {
            durations.pop_front();
        }
        
        auto averageDuration = [&] () {
            return std::accumulate(durations.begin(), durations.end(), 0.0) / durations.size();
        }();
        
        std::printf("Timer(%s) %.4fs %.1ffps\n", timerName.c_str(), averageDuration, 1.0/averageDuration);
    }
}
