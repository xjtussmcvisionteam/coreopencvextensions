//
//  jzplib_draw.cpp
//  OPENCV_HOTSHOTS
//
//  Created by Zhiping Jiang on 14-7-18.
//
//

#include "jzplib_draw.h"

namespace cve {
    
    void drawBox(cv::Mat& image, CvRect box, cv::Scalar color, int thick){
        rectangle( image, cvPoint(box.x, box.y), cvPoint(box.x+box.width,box.y+box.height),color, thick);
    }

    void drawRotatedRect(cv::Mat image, cv::RotatedRect eyeRRect) {
        cv::Point2f rect_points[4];
        eyeRRect.points( rect_points );
        for( int j = 0; j < 4; j++ )
            line( image, rect_points[j], rect_points[(j+1)%4], cv::Scalar(0,0,255), 1, 8 );
    }
    
    void drawStringAtPoint(cv::Mat img, const std::string text, cv::Point2i position) {
        cv::putText(img,text, cv::Point(position.x+1,position.y+1), cv::FONT_HERSHEY_SIMPLEX,0.5f,
                cv::Scalar::all(0),1,CV_AA);
        cv::putText(img,text, cv::Point(position.x,position.y), cv::FONT_HERSHEY_SIMPLEX,0.5f,
                cv::Scalar::all(255),1,CV_AA);
    }
    
    void drawStringAtTopLeftCorner(cv::Mat img, const std::string text) {
        cv::Size2i size = cv::getTextSize(text, cv::FONT_HERSHEY_TRIPLEX, 1.0f, 1, NULL);
        drawStringAtPoint(img, text, cv::Point2i(1,size.height+1));
    }
}
